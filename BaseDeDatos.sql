-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.11-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando datos para la tabla pruebadevisson.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando datos para la tabla pruebadevisson.migrations: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2019_08_19_000000_create_failed_jobs_table', 1),
	(3, '2020_04_16_013646_create_usuarios_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando datos para la tabla pruebadevisson.users: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Devisson', 'devissonvasquez99@gmai.com', NULL, '$2y$10$QsYlIy45TJSGUUVfThrETu8xZkHPOqiLNonmnG5fJg9apxwJPRZWi', 'dMusQ6PDkJsEK1zv8vdBPRA3BdMfOMCjNYVo0hobeRs5IZ69kYaLrHNiB3u5', '2020-04-16 05:35:51', '2020-04-16 05:35:51'),
	(3, 'Alejadra', 'alejarivera155@gmail.com', NULL, '$2y$10$9SV8ldUDSlRR/LHW1XnKU.uebsBSrBpxScvVgVA97VJHgCV3lS7Ee', NULL, '2020-04-17 04:19:38', '2020-04-17 04:19:38'),
	(4, 'Camilo', 'camilo@gmail.com', NULL, '$2y$10$Y8giyM2LmLNWhc8/3bXzw.YaOMeU1EgOgdeIXTKOQyoV4g0gS81ly', NULL, '2020-04-17 04:22:05', '2020-04-17 04:22:05'),
	(5, 'Laura', 'laura@gmail.com', NULL, '$2y$10$kRboumVSjM3Ox7N5HIME/e6YDRW9izaKWHE//M.tO0DF/FWlHf1jS', NULL, '2020-04-17 04:27:37', '2020-04-17 04:27:37'),
	(6, 'John', 'john@gmail.com', NULL, '$2y$10$w00/Tu4DiVzUPbcmfs3qxO.x3jbnG9dsjniOd.HSd.DHu6ly3g2SG', NULL, '2020-04-17 05:01:26', '2020-04-17 05:01:26');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando datos para la tabla pruebadevisson.usuarios: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `Nombre`, `Apellido`, `Documento`, `Correo`, `Rol`, `Direccion`, `created_at`, `updated_at`, `remember_token`) VALUES
	(2, 'David', 'Garcia', 1234566, 'garcia@prueba.com', NULL, 'call 32f # 81-47', NULL, '2020-04-17 05:10:12', NULL),
	(7, 'Alejandra', 'Rivera', 2147483647, 'alejarivera@gmail.com', NULL, 'calle 67 # 49-30', NULL, NULL, NULL),
	(8, 'Laura', 'corrales', 1214747, 'laura@prueba.com', NULL, 'calle 67 # 49-30', NULL, NULL, NULL),
	(9, 'sebastian', 'velez', 1214747084, 'Prueba@prueba.com', NULL, 'calle 67 # 49-23', NULL, NULL, NULL),
	(10, 'camilo', 'perez', 1214748082, 'perez@prueba.com', NULL, 'calle 67 # 64-30', NULL, NULL, NULL),
	(11, 'mailcol', 'Rodriguez', 1234572632, 'maicol@prueba.com', NULL, 'calle 11 # 49-30', NULL, '2020-04-17 05:10:38', NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
