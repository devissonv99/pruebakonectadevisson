
@extends('layouts.app')
@section('content')

<div class="container">
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
        {{Session::get('Mensaje')}}
    </div>
@endif

<a href=" {{ url('usuarios/create')}}" class="btn btn-success" >Agregar Clientes</a>

<br>
<br>

<div class="form-group">
 <input type="text" class="form-control pull-right" style="width:20%" id="search" placeholder="Buscar">
</div>
<table class="table table-light table-hover" id="Tusuarios">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Documento</th>
            <th>Correo</th>
            <th>Dirección</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($usuarios as $usuario)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td> {{$usuario->Nombre}} </td>
                <td> {{$usuario->Apellido}}  </td>
                <td> {{$usuario->Documento}} </td>
                <td> {{$usuario->Correo}} </td>
                <td> {{$usuario->Direccion}} </td>
                <td><a class="btn btn-warning" href="{{ url('/usuarios/'.$usuario->id.'/edit')}}">
                        Editar
                    </a> 
                    <form method="post" action="{{ url('/usuarios/'. $usuario->id)}}" style="display:inline">        
                    {{ csrf_field() }}
                    {{ method_field('DELETE')}}
                    <button class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
                    </form>    
            
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<script>
 // Write on keyup event of keyword input element
    $(document).ready(function(){
    $("#search").keyup(function(){
    _this = this;
    // Show only matching TR, hide rest of them
        $.each($("#Tusuarios tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
                $(this).hide();
            else
                $(this).show();
            });
        });
    });
</script>


<!-- {{ $usuarios->links() }} -->
</div>
@endsection