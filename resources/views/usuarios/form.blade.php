
<div class="form-group">
    <label for="Nombre" class="control-label">{{'Nombre'}} </label>
    <input class="form-control   {{$errors->has('Nombre')?'is-invalid':''}}" type="text" name="Nombre" id="Nombre" value="{{ isset($usuario->Nombre)? $usuario->Nombre:'' }}">
</div>
    
<div class="form-group">
    <label for="Apellido" class="control-label">{{'Apellido'}} </label>
    <input class="form-control {{$errors->has('Apellido')?'is-invalid':''}}" type="text" name="Apellido" id="Apellido"  value="{{ isset($usuario->Apellido)? $usuario->Apellido:'' }}">
</div>

<div class="form-group">
    <label for="Documento" class="control-label">{{'Documento'}} </label>
    <input class="form-control {{$errors->has('Documento')?'is-invalid':''}}" type="number" name="Documento" id="Documento" value="{{ isset($usuario->Documento)? $usuario->Documento:'' }}">
</div>

<div class="form-group">
    <label for="Correo" class="control-label">{{'Correo'}} </label>
    <input class="form-control {{$errors->has('Correo')?'is-invalid':''}}" type="email" name="Correo" id="Correo" value="{{ isset($usuario->Correo)? $usuario->Correo:'' }}">
</div>
    

<div class="form-group">
    <label for="Direccion" class="control-label">{{'Direccion'}} </label>
    <input class="form-control {{$errors->has('Direccion')?'is-invalid':''}}" type="text" name="Direccion" id="Direccion" value="{{ isset($usuario->Direccion)? $usuario->Direccion:'' }}">
</div>

<div class="form-group">
    <input type="submit" class="btn btn-success" value="{{ $Modo=='crear' ? 'Agregar':'Modificar' }}">
    <a class="btn btn-primary" href=" {{ url('usuarios')}}">Regresar</a>
</div>